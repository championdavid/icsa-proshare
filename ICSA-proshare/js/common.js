// Global variables + functions
var pageLoaded = false;
var rootHtml = $('html');
var rootBody = $('body');
var burger = $('#menu--trigger');

function owlSlider() {

    var owl = $('.home-slider');
    owl.owlCarousel({
        items: 1,
        loop: true,
        center: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: false,
        nav: false,
        dots: false
    });

}

function logoSlider() {

    var logoOwl = $('.logo-slider');
    logoOwl.owlCarousel({
        loop: true,
        center: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 7700,
        autoplayHoverPause: false,
        nav: false,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                center: false
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1260: {
                items: 4
            }
        }
    });

}

function initNavSetup() {

    var burgerIcon = $("#menu--trigger > span");

    burger.click(function () {
        burgerIcon.toggleClass("menu-is-open");
    });

    var menuOpen = false;

    burger.click(function () {
        if (!menuOpen) {
            $('nav').addClass("nav--open");
            menuOpen = true;
        } else {
            $('nav').removeClass("nav--open");
            menuOpen = false;
        }
    });

}

// Jquery initialisation
$(function () {

    // On load
    $(window).on("load", function () {
        pageLoaded = true;
        if (pageLoaded) {
            rootHtml.addClass("app");
        }
    });

    // Nav 
    initNavSetup();

    // Resize
    $(window).on("resize", function () {
     
    });

    // Sliders
    owlSlider();
    logoSlider();

});