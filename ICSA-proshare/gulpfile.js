﻿/// <binding AfterBuild='default' ProjectOpened='watch' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace');
    minify = require('gulp-minify');

// Compile sass
gulp.task('sass', function () {
    gulp.src('./scss/**/*.scss')
        //.pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});

// Watch the sass folder for changes
gulp.task('watch', ['sass'], function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
});

// Compile javascript
gulp.task('js', function () {
    gulp.src(['./js/common.js'])
        .pipe(minify())
        .pipe(gulp.dest('./js'));
});

// All in one build task - Visual studio will run this automagically.
gulp.task('default', ['sass', 'js'], function () {
});